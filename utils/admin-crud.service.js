(function() {
	const mongoose = require('mongoose').connect('mongodb://localhost/test');
	const Admin = require('../api/authentication/admin.model');

	function getAllAdmins(res) {
		Admin.find(function(err, admins) {
			if (err) console.log(err);

			console.log(admins)
		});
	}

	function addAdmin(username, password) {
		var admin = new Admin();

		admin.username = username;
		admin.password = password;

		admin.save(function(err) {
			if (err) {
				if (err.code == 11000)
					console.log("Dupes!");
				else
					console.log(err);
			}

			console.log('Created: ', admin.username);
		})
	}

	function deleteAdmin(username) {
		Admin.remove({
			username: username
		}, function(err, player) {
			if (err) console.log(err);

			console.log('Deleted: ', username);
		})
	}

	module.exports = {
		getAllAdmins: getAllAdmins,
		addAdmin: addAdmin,
		deleteAdmin: deleteAdmin
	}
})();