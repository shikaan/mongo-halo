(function() {
	const OCRAD = require('../../vendor/ocrad.js');
	const Canvas = require('canvas');
	const {Caman} = require('caman');
	const Image = Canvas.Image;
	const fs = require('fs');
	const conf = require('./recognize.conf');
	const {updateRecords} = require('./update-records.service');
	const logger = require('../common/logging.service');
	const {UnhandledException} = require('../common/exception.model')

	function _recognizeImage(successCallback, failureCallback) {
		fs.readFile(conf.temporaryRenderPath, (err, src) => {
			if (err) throw err;

			var img = new Image();
			img.src = src;

			/**
			 * DIRTY HACK: sometimes the photo isn't loaded whenyou try to 
			 * recognize it. So we wait some seconds...
			 */
			setTimeout(() =>{
				try{
					successCallback(_recognizeWorker(img));
				}
				catch(e){
					logger.error(`Error while recognizing photo: ${e}`);
					failureCallback(e)
				}
			}, 5000);
		});
	}

	function _recognizeWorker(img){
		logger.debug("Starting recognize...");
		var canvas = new Canvas(img.width, img.height);
		var ctx = canvas.getContext('2d');
		ctx.drawImage(img, 0, 0, img.width, img.height);

		let RENDERED_DATA = OCRAD(canvas);
		logger.info("Finished recognize");
		logger.debug(RENDERED_DATA);
		return RENDERED_DATA
	}

	function getDataFromTempRaw() {
		logger.debug("Starting photo binarize...")
		return new Promise((resolve, reject) => {
			Caman(conf.temporaryRawPath, function() {
				this.greyscale()
					.invert()
					.contrast(100)
					.render(function() {
						logger.info("Photo binarizing complete");
						this.save(conf.temporaryRenderPath);

						_recognizeImage(resolve, reject);
					});
			})
		})
	}

	module.exports = {
		getDataFromTempRaw: getDataFromTempRaw
	}
})();