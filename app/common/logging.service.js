(function(){
    "use strict";
    const colors = require('colors');
    const args = require('optimist').argv;    

    let LOG_LEVELS = {
        DEBUG: 0,
        INFO: 1,
        WARNING: 2,
        ERROR: 3
    }

    let logLevel = _setLogLevel();
    console.log(colors.green(`Currently logging level: ${logLevel}`));

    function _setLogLevel(){
        if(typeof (args.l || args.log) !== 'undefined') {
            let level = parseInt(args.l || args.log);
            if(!isNaN(level) && level >= LOG_LEVELS.DEBUG && level <= LOG_LEVELS.ERROR){
                return level
            }
        }
        
        return LOG_LEVELS.INFO;
    }

    function debug(){
        if(logLevel === LOG_LEVELS.DEBUG)
            console.log((new Date()).toLocaleTimeString(), colors.grey.apply(this, arguments))
    } 

    function info(){
        if(logLevel <= LOG_LEVELS.INFO){
            console.log('--- INFO ---'.bgBlue.white.bold);
            console.log((new Date()).toLocaleTimeString(), colors.blue.apply(this, arguments));
        }
    }

    function error(){
        if(logLevel <= LOG_LEVELS.ERROR){
            console.log('--- ERROR ---'.bgRed.white.bold);
            console.log((new Date()).toLocaleTimeString(), colors.red.apply(this, arguments));
        }
    }

    function warning(){
        if(logLevel <= LOG_LEVELS.WARNING){
            console.log('--- WARNING ---'.bgYellow.white.bold);
            console.log((new Date()).toLocaleTimeString(), colors.yellow.apply(this, arguments));
        }
    }

    module.exports = {
        info: info,
        error: error,
        warning: warning,
        debug: debug
    }
})()