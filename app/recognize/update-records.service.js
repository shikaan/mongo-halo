(function(){
    var Player = require('../players/player.model');
    const logger = require('../common/logging.service');
    const {
        ConflictException, 
        UnhandledException, 
        NotFoundException
    } = require('../common/exception.model');
    const SCORE = [3,2,1,0];
    const PLAYERS_BOUND = SCORE.length;

    function _getAllPlayers(){
        logger.debug('Fetching all players list');
		let query = Player.find();

        return new Promise(function(resolve, reject){
            query.exec(function(err, players) {
                if (err) 
                    reject(new UnhandledException(err));

                logger.info('Fetched players list');
                logger.debug(players);

                resolve(players)
            })
        })
	}

    function _getPlayersData(players, data){
        let aData = data.split(/\r?\n/);
        let positions = [];
        logger.debug("Populating positions list...");
        players.forEach(function(player){
            aData.forEach(function(entry){
                if(entry.includes(player.nickname)){
                    positions.push(player._id); 
                }
            })
        })

        return new Promise(function(resolve, reject){
            if(positions.length === PLAYERS_BOUND){
                let promiseChain = [];
                logger.debug("Updating database...");
                positions.forEach(function(id, position){
                    promiseChain.push(_writePlayerData(id, position))
                })
                logger.info("Updated database");
                resolve(Promise.all(promiseChain))
            }
            else{
                logger.warning(`Not updating records: \n Expected ${PLAYERS_BOUND} players, got ${positions.length} instead`)
                reject(new ConflictException(`Expected ${PLAYERS_BOUND} players, got ${positions.length} instead`))
            }
        })
        
    }

    function _writePlayerData(id, position){
        /**
        *   TODO: make this function save on db ONLY when all player are updated.
        *   As of now, if we get an error only on the last player, all other
        *   players will be updated.
        */
        return new Promise(function(resolve, reject){
            Player.findById(id, function(err, player){
                if(err) 
                    reject(new UnhandleException(err));

                if(!player)
                    reject(new NotFoundException('Player not found'));

                let pointsToBeAdded = SCORE.length >= Number(position) ? SCORE[position] : 0;
                
                player.points = player.points + pointsToBeAdded;
                _updateStats(player, position);

                logger.debug(`Updated ${player.nickname}: new score ${player.points}`);

                player.save(function(err){
                    if(err) {
                        logger.error(`Error while updating ${player.nickname}`)
                        reject(new UnhandledException(err));
                    }

                    resolve()
                })
            })
        })
    }

    function _updateStats(player, position){
        switch(Number(position)){
            case 0:
                player.victories++;
                break;
            case 1:
                player.secondPlaces++;
                break;
            case 2:
                player.thirdPlaces++;
                break;
            case 3:
                player.fourthPlaces++;
                break;
        }
    }

    function updateRecords(data){
        return _getAllPlayers()
            .then(
                (players) => _getPlayersData(players, data),
                (exception) => exception
            )
    }

    module.exports = {
        updateRecords: updateRecords
    }
})();