(function(){
	const jwt = require('jsonwebtoken');
    const secret = process.env.SECRET || 'Badu';

    function authenticate(req, res, next){
    	if(req.method == 'OPTIONS'){
            //Ability to handle CORS...
            next();
        }
        else{
           var token = req.body.token || req.headers['x-access-token'];

            if(token){
                jwt.verify(token, secret, function(err, decoded){
                    if(err){
                        return res.status(401).send({
                            success: false,
                            message: 'Unauthorized'
                        })
                    }
                    else{
                        req.decoded = decoded;
                        next();
                    }
                })
            }
            else{
                return res.status(401).send({
                    success: false,
                    message: 'Token unfound'
                })
            }
        }
    }

    module.exports = {
    	authenticate: authenticate
    }
})();