(function(){
	const secret = 'Badu';
	const DB_URL = 'mongodb://localhost/test';
	require('./help.config');

	function setup(app){
		const bodyParser = require('body-parser');
		const morgan = require('morgan');
		const mongoose = require('mongoose');

		mongoose.connect(DB_URL);

		app.use(bodyParser.urlencoded({extended: true}));
		app.use(bodyParser.json());

		app.use(function(req, res, next) {
			res.setHeader('Access-Control-Allow-Origin', '*');
			res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
			res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization');
			next();
		});
		
		app.use(morgan('dev'));
	};

	module.exports = {
		setup: setup,
		secret: secret,
		DB_URL: DB_URL
	}
})();