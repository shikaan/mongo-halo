(function(){
	module.exports = function(app){
		const express = require('express');
        const adminRouter = express.Router();
        const adminService = require('./authentication.service');

        adminRouter
            .post('/', function(req, res) {
                adminService.authenticateAdmin(req, res);
            })

        app.use('/api/authentication', adminRouter);
	}
})();