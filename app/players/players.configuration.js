(function(){
	const MODEL_FIELDS = ['name', 'nickname', 'victories', 'secondPlaces', 'thirdPlaces', 'fourthPlaces', 'points'];

	module.exports = {
		MODEL_FIELDS: MODEL_FIELDS
	}
})();