(function(){
    const args = require('optimist').argv;

    if(args.help || args.h){
        let version = require('../package.json')
        console.log(
            `MONGO-HALOIDE version: ${version}`,
            `A simple backend to update a MongoDB powered players\' database by optical recognition`
        ) 
        console.log('Usage:');
        console.log('\t-l, --log\tLogging level: from 0 (debug) to 3 (errors only)' )
        console.log('\t\t\tDefaults to 1 (info)' )
        process.exit(0);
    }
})()