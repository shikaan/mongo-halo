(function(){
    const chai = require('chai');
    const sinon = require('sinon');
    const expect = chai.expect;
    const assert = chai.assert;
    const service = require('./upload-photo.service'); 

    describe('Service: UploadPhoto', function() {
        it('should be defined', function() {
            expect(!!service).to.equal(true);
        });
    });
})();