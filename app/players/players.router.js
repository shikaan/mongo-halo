(function(){
    module.exports = function(app) {
        const express = require('express');
        const apiRouter = express.Router();
        const middleware = require('../authentication.middleware');
        const service = require('./players.service'); 

        apiRouter.use(function(req, res, next){
            middleware.authenticate(req,res,next);
        })

        apiRouter.route('/players')
            .post(function(req, res){
                let payload = req.body;
                service.addPlayer(payload,res);
            })
            .get(function(req, res){
                service.getAllPlayers(res);
            })

        apiRouter.route('/players/:player_id')
            .get(function(req,res){
                service.getPlayer(req.params.player_id, res);
            })
            .put(function(req, res){
                service.updatePlayer(req.params.player_id, req, res);
            })
            .delete(function(req,res){
                service.removePlayer(req.params.player_id, res);
            })

        app.use('/api', apiRouter);
    };
})();