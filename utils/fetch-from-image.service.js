(function() {
	const OCRAD = require('../vendor/ocrad.js');
	const Canvas = require('canvas');
	const {Caman} = require('caman');
	const Image = Canvas.Image;
	const fs = require('fs');

	const TEMP_RENDER_FILE = './temp_render.png';
	let RENDERED_DATA = '';

	function _recognizeImage() {
		fs.readFile(TEMP_RENDER_FILE, (err, src) => {
			if (err) throw err;

			var img = new Image();
			img.src = src;

			var canvas = new Canvas(img.width, img.height);
			var ctx = canvas.getContext('2d');
			ctx.drawImage(img, 0, 0, img.width, img.height);

			console.log(OCRAD(canvas));
			_deleteTempRender()
		});
	}

	function _deleteTempRender() {
		fs.unlink(TEMP_RENDER_FILE, (err) => {
			if (err) throw err;
		});
	}

	function fetchDataFromFile(filePath) {
		Caman(filePath, function() {
			this.greyscale()
				.invert()
				.contrast(100)
				.render(function() {
					this.save(TEMP_RENDER_FILE);

					_recognizeImage();
				});
		})
	}

	module.exports = {
		fetchDataFromFile: fetchDataFromFile,
		data: RENDERED_DATA
	}
})();