(function() {
    class Exception{
        constructor(){}
        sendResponse(response){
            return response.status(this.status).send({message: this.message});
        }
    }

    class ConflictException extends Exception{
        constructor(message){
            super();
            this.status = 409;
            this.message = message || 'Conflict'
        }
    }

    class UnhandledException extends Exception{
        constructor(message){
            super();
            this.status = 500;
            this.message = message || 'Unhandled';
        }
    }

    class AuthorizationException extends Exception{
        constructor(message){
            super();
            this.status = 401;
            this.message = message || 'Unauthorized'
        }
    }

    class NotFoundException extends Exception{
        constructor(message){
            super();
            this.status = 404;
            this.message = message || 'Not found'
        }
    }

    module.exports = {
        AuthorizationException: AuthorizationException,
        UnhandledException: UnhandledException,
        ConflictException: ConflictException,
        NotFoundException: NotFoundException,
        Exception: Exception
    }
})()