(function() {
	var mongoose = require('mongoose');
	var Schema = mongoose.Schema;

	var PlayerSchema = new Schema({
		name: String,
		nickname: {
			type: String,
			required: true,
			index: {
				unique: true
			}
		},
		victories: {
			type: Number,
			required: false
		},
		secondPlaces: {
			type: Number,
			required: false
		},
		thirdPlaces: {
			type: Number,
			required: false
		},
		fourthPlaces: {
			type: Number,
			required: false
		},
		points: {
			type: Number,
			required: false
		}
	})

	module.exports = mongoose.model('Player', PlayerSchema);
})();