(function(){
	
	module.exports = function(app){
		const express = require('express');
        const apiRouter = express.Router();
        const middleware = require('../authentication.middleware');
        const controller = require('./recognize.controller');

        apiRouter.use(function(req, res, next){
            middleware.authenticate(req,res,next);
        })

        apiRouter.route('/recognize')
            .post(function(req, res){
                controller(req, res);
            })

        app.use('/api', apiRouter);
	}
})();
