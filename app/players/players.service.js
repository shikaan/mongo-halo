(function(){
	"use strict";

	var Player = require('./player.model');
	var conf = require('./players.configuration');
	const logger = require('../common/logging.service');
	const {ConflictException, NotFoundException, UnhandledException} = require('../common/exception.model');

	function addPlayer(payload, res){
		var player = new Player();

		conf.MODEL_FIELDS.forEach((field) => {
			if(payload.hasOwnProperty(field))
				player[field] = payload[field];
			else{
				player[field] = 0;
				logger.debug('Missing filed in request: ', field);
			}
		})

		player.save(function(err){
			if(err) {
				if(err.code == 11000){
					logger.warning('Duplicate entry: ', player.nickname);
					return (new ConflictException("Dupes!")).sendResponse(res);
				}
				else{
					logger.error('Error while saving player: ', player.nickname);
					logger.debug(err);
					return (new UnhandledException(err)).sendResponse(res);
				}
			}

			logger.info('Added player: ', player.nickname);
			res.status(201).send(player);
		})
	}

	function getPlayer(id, res){
		Player.findById(id, function(err, player){
			if(err){
				logger.error('Error while retrieving player: ', id);
				logger.debug(err);
				return (new UnhandledException(err)).sendResponse(err);
			}
			logger.info('Fecthed player: ', player.nickname);
			res.json(player);
		})
	}

	function updatePlayer(id, req, res){
		Player.findById(id, function(err, player){
			if(err){
				logger.error("Error while fetching player: ", id);
				logger.debug(err);
				return (new UnhandledException(err)).sendResponse(res);
			}

			if(!player){
				logger.warning("Player not found: ", id);
				return (new NotFoundException("Player not found: " + id)).sendResponse(res);
			}

			conf.MODEL_FIELDS.forEach((field) => {
				if(typeof req.body[field] !== 'undefined' && req.body[field] != player[field])
					player[field] = req.body[field];
			})

			player.save(function(err){
				if(err){
					logger.error('Error while saving player: ', player.nickname);
					logger.debug(err);
					return (new UnhandledException(err)).sendResponse(res);
				}
				logger.info('Updated player: ', player.nickname);
				res.json(player);
			})

		})
	}

	function removePlayer(id, res){
		Player.remove({_id: id }, function(err, player){
			if(err) {
				logger.error('Error while removing player: ', id);
				logger.debug(err);
				return res.status(500).send(err);
			}

			if(!player){
				res.status(404).send('Player not found');
				logger.warning("Player not found: ", id);
			}

			logger.info("Removed player: ", player.nickname);
			res.status(204).send();
		})
	}

	function getAllPlayers(res){
		Player.find(function(err, players) {
            if(err) {
				logger.error('Error while removing player: ', id);
				logger.debug(err);
				return res.status(500).send(err);
			}

			logger.info("Fecthed all players");
            res.json(players);
        });
	}

	module.exports = {
		addPlayer: addPlayer,
		getPlayer: getPlayer,
		removePlayer: removePlayer,
		updatePlayer: updatePlayer,
		getAllPlayers: getAllPlayers
	}
})();