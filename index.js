const express = require('express');
const app = express();
const PORT = process.env.PORT || 8080;

require('./config').setup(app);

app.get('/', function(req, res){
	res.send('Welcome to the home page')
});

require('./app/authentication')(app);
require('./app/players')(app);
require('./app/recognize')(app);

app.listen(PORT);
console.log(new Date().toLocaleTimeString(), '- Server started on port: ', PORT);
