(function(){
    const Admin = require('./admin.model');
    const jwt = require('jsonwebtoken');
    const secret = require('../../config').secret;
    const logger = require('../common/logging.service');
    const {AuthorizationException, NotFoundException} = require('../common/exception.model');

    /**
     * Authenticates an user and in case of success provides a json web token
     * @param  {[type]} req  [description]
     * @param  {[type]} res  [description]
     */
    function authenticateAdmin(req, res){
        Admin.findOne({username: req.body.username})
            .select('name username, password')
            .exec(function(err, user){
                if(err){
                    logger.error('Unable to read: ', req.body.username);
                    logger.debug(err);
                    return (new UnhandledException(err)).sendResponse(res);
                }
                //Unfound user
                if(!user){
                    logger.error('Unable to find admin: ', req.body.username);
                    return (new AuthorizationException()).sendResponse(res);
                }
                else{
                    var isValidPassword = user.comparePassword(req.body.password);
                
                    if(!isValidPassword){
                        logger.error('Wrong password for user ', user.username);
                        return (new AuthorizationException()).sendResponse(res);
                    }
                    else{
                        var token = jwt.sign(user, secret, {expiresIn: '24h'});
                        
                        logger.debug('Obtained token for user', user.username);
                        res.status(200).json({
                            token: token
                        })
                    }
                }
            })
    }

    module.exports = {
        authenticateAdmin: authenticateAdmin
    }
})();