(function(){
    const {uploadPhoto} = require('./upload-photo.service');
    const {getDataFromTempRaw} = require('./fetch-photo-data.service');
    const {updateRecords} = require('./update-records.service');
    const {Exception, UnhandledException} = require('../common/exception.model');
    const logger = require('../common/logging.service');

    function _deleteTempFiles() {
		fs.unlink(conf.temporaryRawPath, (err) => {
			if (err) throw err;
		});
		fs.unlink(conf.temporaryRenderPath, (err) => {
			if (err) throw err;
		});
	}

    function recognizeController(request, response){
        logger.debug('Update photo')
        uploadPhoto(request)
            .then(() => {
                logger.debug('Get Data from Temp Raw')
                return getDataFromTempRaw()
            })
            .then((data) => {
                logger.debug('Update records')
                return updateRecords(data);
            })
            .then(() => {
                response.status(200).send();
            })
            .catch((exception) => {
                console.log(exception)
                if(exception instanceof Exception)
                    return exception.sendResponse(response)
                else
                    return (new UnhandledException()).sendResponse(response);
            })
    }

    module.exports = recognizeController;
})();