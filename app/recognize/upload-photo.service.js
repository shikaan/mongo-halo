(function(){
	const {IncomingForm} = require('formidable');
	const conf = require('./recognize.conf');
	const path = require('path');
	const fs = require('fs');
	const logger = require('../common/logging.service');

	function uploadPhoto(request){
		return new Promise((resolve, reject) => {
				try{
			let form = new IncomingForm();
		
			form.multiples = false;
			form.uploadDir = conf.uploadDirectory;

			form.on('file', function(field, file) {
				fs.renameSync(file.path, conf.temporaryRawPath);
				logger.info(`Received file...`)
				resolve();
			});

			form.on('error', function(err) {
				logger.error(`Error while uploading photo: ${err}`);
				reject();
			});

			form.parse(request);
		}
		catch(e){
			reject(e);
		}
			})
	}
	
	module.exports = {uploadPhoto}
})()
