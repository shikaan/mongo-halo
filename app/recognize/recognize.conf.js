(function(){
    const path = require('path');

    const temporaryRawName = 'temp_raw.png';
    const temporaryRenderName = 'temp_render.png';
    const uploadDirectory = 'utils';

    module.exports = {
        uploadDirectory: uploadDirectory,
        temporaryRawPath: path.join(uploadDirectory, temporaryRawName),
        temporaryRenderPath: path.join(uploadDirectory, temporaryRenderName)
    }
})()